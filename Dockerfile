FROM ansible/awx:17.0.1

LABEL maintainer="anders.harrisson@esss.se"

USER 0

# Add icsv-awx01.esss.lu.se/pss-bastion-01.tn.esss.lu.se to known_hosts
# The reason for this is to allow ssh tunneling to GPN/PSS.
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    echo "icsv-awx01.esss.lu.se,10.0.42.90 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBD9ymnuDd9kZELHPtn3Z2I9QFfvtac+F3rCWWSo+Egb1xkUD48RhhO0rzIABGaLw1ohou88wPXngxcglCihRtjk=" > /root/.ssh/known_hosts && \
    echo "pss-bastion-01.tn.esss.lu.se,172.16.50.11 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAJmCftMjFAeXR0e/Ngly12uY/6uB0fjytnyCP2fy6dYzklnCzzwRKgVcOaUwYLIQqa+dSW6NxGtQrx+FfLUzlY=" >> /root/.ssh/known_hosts &&\
    echo "safety-fw-01.cslab.esss.lu.se ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCtQCCBlhSXmHd6weDbLedcs6F2P+tP9X1p0l5AtHJcVfpMNy/8zxIhXlDslE4TLiSPBsimj/WB7VFFUuBuKOiQ=" >> /root/.ssh/known_hosts

# Environment variables to customize ansible configuration
ENV ANSIBLE_TRANSFORM_INVALID_GROUP_CHARS=ignore

COPY nginx.conf /etc/nginx/nginx.conf

# Install ESS internal CA Certificate
ADD http://artifactory.esss.lu.se/artifactory/certificates/esss-ca01-ca.cer /etc/pki/ca-trust/source/anchors/
RUN chmod 0755 /etc/pki/ca-trust/source/anchors/esss-ca01-ca.cer && \
    update-ca-trust

RUN /var/lib/awx/venv/ansible/bin/pip install --no-cache-dir \
  -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple \
  proxmoxer \
  cerberus==1.3.2 \
  jxmlease \
  netaddr \
  ncclient \
  jmespath \
  csentry-api==1.0.1 \
  csentry-inventory==1.0.0 \
  hvac

USER 1000
